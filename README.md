Description
-----------
	  
DrawingTool is simple graphics tool allowing users to display works of
art on a console. Users can draw lines and rectangles and fill in
sections of the work of art with a colour of their choice. 

Deployment instructions
------------
Install at least Java 6
http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html

Execute Jar
Java -jar DrawingTool.jar

Unit Tests
Install Maven
http://maven.apache.org/download.cgi

mvn test

Instructions
------------
DrawingTool has the following commands:

C w h 		Create a new canvas of width w and height h.

L x1 y1 x2 y2 	Create a new line from (x1,y1) to (x2,y2). Currently only 
		horizontal or vertical lines are supported. Horizontal and 
		vertical lines will be drawn using the 'x' character.
				
R x1 y1 x2 y2 	Create a new rectangle, whose upper left corner is 
		(x1,y1) and	lower right corner is (x2,y2). Horizontal and 
		vertical lines will be drawn using the 'x' character.
				
B x y c 	Fill the entire area connected to (x,y) with "colour" 
		c. The behavior of this is the same as that of the "bucket 
		fill" tool in paint programs.

Q 		Quit the program.


Development
-----------
Java:			6
IDE:			Eclipse JEE: Luna Release (4.4.0)
Management Tool:	Apache Maven 3.2.3
Dependencies:		JUnit 4.11
			hamcrest-core-1.3

Git Access
----------
https://bitbucket.org/Lampshady/drawingtool/overview
	
 
Contacts
--------
Author: Zach Goldstein
Email:	zakGoldstein24@gmail.com