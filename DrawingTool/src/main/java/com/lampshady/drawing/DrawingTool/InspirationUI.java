package com.lampshady.drawing.DrawingTool;

import java.util.Scanner;

public class InspirationUI {
	public static final int MAXARGS = 5;
	private Scanner scan;
	
	/*	Prompts user for input and validates parameters provided
	 * 	Prompting will loop if invalid input is entered
	 */
	public String[] input() {
		boolean isValid = false;
		String [] args= null;
		String input;
		
		System.out.print("enter command:");
		scan = new Scanner(System.in);
		do{
			input = scan.nextLine();
			args = input.split("\\s+");
			
			try{
				isValid = validateInput(args);
			} catch (IllegalArgumentException e) {
				System.out.println(e.getMessage());
			}
		}while(!isValid);
		
		
		return args;
	}

	/*	Validates the user provided an acceptable command and
	 * 	the user provided acceptable parameters to accompany 
	 * 	the command
	 */
	protected boolean validateInput(String[] parameters)
			throws IllegalArgumentException {
		char command;

		// Validates User Command
		if (parameters.length < 1)
			throw new IllegalArgumentException(
					"Input Error: No Arguments Provided.");
		if (parameters[0].length() != 1)
			throw new IllegalArgumentException("Input Error: Invalid Command.");

		command = Character.toUpperCase(parameters[0].charAt(0));


		// Checks Parameter size for each input option
		int paramLen = parameters.length;
		switch (command) {
		case Artist.LINE:
		case Artist.RECTANGLE:
			if (paramLen != 5)
				throw new IllegalArgumentException("Input Error: Five Arguments Required.");
			break;
		case Artist.BUCKET:
			if (paramLen != 4)
				throw new IllegalArgumentException("Input Error: Four Arguments Required.");
			if(parameters[Artist.COLOURINDEX].length() != 1) throw new IllegalArgumentException("Input Error: Invalid Colour.");
			break;
		case Artist.CREATE:
			if (paramLen != 3)
				throw new IllegalArgumentException("Input Error: Three Arguments Required.");
			break;
		case Artist.QUIT:
			if (paramLen != 1)
				throw new IllegalArgumentException("Input Error: One Arguments Required.");
			break;
		default:
			throw new IllegalArgumentException("Input Error: '" +command+ "'Invalid Command.");
		}

		// Validates Parameters are numeric values
		for (String arg : parameters) {
			if (arg == parameters[0])continue;
			
			if(Artist.BUCKET==command)
				if (arg == parameters[3])continue;

				
			if (!isValidInt(arg))
				throw new IllegalArgumentException(
						"Input Error: Coordinates must be Positive Numbers.");
		}
		
		return true;
	}

	/* Checks if a string provided is a positive integer
	 */
	private boolean isValidInt(String s) {
		int i;
		try {
			i = Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}

		if (i < 1)
			return false;

		return true;
	}
	
	public void close(){
		scan.close();
	}
}
