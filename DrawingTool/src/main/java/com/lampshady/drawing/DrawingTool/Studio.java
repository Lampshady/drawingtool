package com.lampshady.drawing.DrawingTool;


public class Studio {
	private Artist artist;
	
	public Studio(){
		artist = new Artist();
	}
	
	public void execute(){
		do {
			artist.input();
			artist.draw();
			artist.paint();
	
		}while(!artist.isArtistDone());
		
		System.out.println("Thank you for Drawing!");
	}
   
}
