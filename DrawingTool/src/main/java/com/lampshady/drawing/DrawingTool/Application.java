package com.lampshady.drawing.DrawingTool;


public class Application 
{
	/*	Initializes a studio object and executes it
	 */
    public static void main( String[] args ){
    	Studio studio = new Studio();
    	
    	studio.execute();
    	
    }
}
