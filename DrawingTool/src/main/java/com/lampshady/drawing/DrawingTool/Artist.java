package com.lampshady.drawing.DrawingTool;


public class Artist {
	public static final char CREATE = 'C';
	public static final char LINE = 'L';
	public static final char RECTANGLE = 'R';
	public static final char BUCKET = 'B';
	public static final char QUIT = 'Q';
	public static final int COLOURINDEX = 3;
	
	private InspirationUI inspirationUI;
	private Canvas canvas;
	private Brush brush;

	public Artist() {
		//TODO Hash Table of acceptable input
		canvas=null;
		
	}
	/*	Requests user input through the inspiration UI
	 * 	Builds a brush object based upon valid user input
	 */
	public void input() {
		String[] parameters;
		InspirationUI inspirationUI=new InspirationUI();
		parameters =inspirationUI.input();
		brush = buildBrush(parameters);

	}

	/* The canvas object is created / populated with data from the brush object
	 * 
	 */
	public void draw() {
		//Checks for Create Command
		if (brush.getAction() == Artist.CREATE){
			canvas = new Canvas(brush.getStartPoint());
			return;
		}
		
		//Checks if canvas exists
		if(canvas==null){
			System.out.println("Canvas Error: Canvas Required for Drawing.");
			return;
		}
			
		//Draws based upon user command
		try {
			canvas.validatePoints(brush);
			
			switch (brush.getAction()) {
			case Artist.LINE:
				canvas.drawLine(brush.getColour(), brush.getStartPoint(), brush.getEndPoint());
				break;
			case Artist.RECTANGLE:
				canvas.drawRect(brush.getColour(), brush.getStartPoint(), brush.getEndPoint());
				break;
			case Artist.BUCKET:
				canvas.drawBucket(brush.getColour(), brush.getStartPoint());
				break;
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println(e.getMessage());
			return;
		}

	}

	public void paint() {
		if(canvas==null)return;
		Exhibition.paint(canvas);
	}
	
	private Brush buildBrush(String[] parameters) {
		char command;
		char colour = 'x';
		Point startPoint=null;
		Point endPoint=null;
		
		//Gets and sets char paramters
		command = Character.toUpperCase(parameters[0].charAt(0));
		if(command==BUCKET)colour = parameters[3].charAt(0);
		
		//Gets and sets points
		switch (command) {
		case LINE:
		case RECTANGLE:
			endPoint = new Point(Integer.parseInt(parameters[3])-1,Integer.parseInt(parameters[4])-1);
		case BUCKET:
			startPoint=new Point(Integer.parseInt(parameters[1])-1,Integer.parseInt(parameters[2])-1);
			break;
		case CREATE:
			startPoint=new Point(Integer.parseInt(parameters[1]),Integer.parseInt(parameters[2]));
			break;
		}
		
		
		return new Brush(command, colour, startPoint, endPoint);
	}


	public boolean isArtistDone() {
		return (brush.getAction() == QUIT);
	}
	public void close(){
		inspirationUI.close();
	}
	
	public Brush getBrush() {
		return brush;
	}
	public Canvas getCanvas() {
		return canvas;
	}
	
	



}