package com.lampshady.drawing.DrawingTool;



public class Canvas {
	
	private int height;
	private int width;
	private char[][] drawingArea;
	

	public Canvas(Point point) {
		this.width = point.x;
		this.height = point.y;
		
		buildCanvas();
		
	}

	private void buildCanvas() {
		drawingArea = new char[height][width];
		for (int y=0;y<height;y++)
			for (int x=0;x<width;x++)
				drawingArea[y][x] = ' ';
		
	}

	public void drawLine(char colour, Point point1, Point point2) throws IndexOutOfBoundsException {
		
		
		if(point1.sameX(point2)){
			if(point1.y<point2.y) 	drawVertical(colour, point1.x,point1.y,point2.y);
			else					drawVertical(colour, point1.x,point2.y,point1.y);
		} else if(point1.sameY(point2)){
			if(point1.x<point2.x)	drawHorizontal(colour, point1.y, point1.x, point2.x);
			else					drawHorizontal(colour, point1.y, point2.x, point1.x);
		}
		else 
			throw new IndexOutOfBoundsException("Line Error: Line not Horizontal or Vertical.");
	}


	
	public void drawRect(char colour, Point point1, Point point2) {
		if(!validateRectangle(point1,point2)) 
			throw new IndexOutOfBoundsException("Rectangle Error: Point One is not the Upper Left Corner.");
		
		drawVertical(colour, point1.x,point1.y,point2.y);
		drawVertical(colour, point2.x,point1.y,point2.y);
		drawHorizontal(colour, point1.y,point1.x,point2.x);
		drawHorizontal(colour, point2.y,point1.x,point2.x);
	}
	
	private boolean validateRectangle(Point point1, Point point2) {
		if(point1.x>point2.x || point1.y>point2.y)return false;
			
		return true;
		
	}

	private void drawVertical(char colour, int row, int start, int end){
		for (int y=start;y<=end;y++)
			drawingArea[y][row] = colour;
	}
	
	private void drawHorizontal(char colour, int col, int start, int distance){
		for (int x=start;x<=distance;x++)
			drawingArea[col][x] = colour;
	}
	
	public void drawBucket(char colour, Point point) {
		if(!checkBoundary(point) || !(drawingArea[point.y][point.x]==' '))return;
		
		drawingArea[point.y][point.x] = colour;
		
		drawBucket(colour, point.down());
		drawBucket(colour, point.up());
		drawBucket(colour, point.left());
		drawBucket(colour, point.right());
		
	}

	private boolean checkBoundary(Point point) {
		return !((point.x<0) || (point.x>=width) || (point.y<0) || (point.y>=height));
	}
	
	public void validatePoints(Brush brush) {
		 if(brush.getStartPoint() != null && !checkBoundary(brush.getStartPoint()))
				 throw new IndexOutOfBoundsException("Point Error: Point out of bounds.");
		 if(brush.getEndPoint() != null && !checkBoundary(brush.getEndPoint()))
		 	throw new IndexOutOfBoundsException("Point Error: Point out of bounds.");
		 
		 return;
		
	}

	
	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public char[][] getDrawingArea() {
		return drawingArea;
	}



}

