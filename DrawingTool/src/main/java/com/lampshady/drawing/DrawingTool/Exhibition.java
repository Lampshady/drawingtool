package com.lampshady.drawing.DrawingTool;

public class Exhibition {
	public static final char BUCKETCHAR = '*';
	public static final char HORIZONTALCHAR = '-';
	public static final char VERTICALCHAR = '|';
	
	/*	Prints a 2D array to screen with a character frame
	 */
	public static void paint(Canvas canvas) {
		char[][] art = canvas.getDrawingArea();
		int width = canvas.getWidth();
		
		printHorizontalLine(width);
		for (char[] row : art){
			System.out.print("|");
			for (char pixel : row)
				System.out.print(" "+pixel+" ");
			System.out.println("|");
		}
		printHorizontalLine(width);
	}

	/*	Prints a straight horizontal line with '-' charters
	 * 	The length of the line printed is width
	 */
	private static void printHorizontalLine(int width) {
		System.out.print(" ");
		for (int x=0;x<width;x++)System.out.print(" "+HORIZONTALCHAR+" ");
		System.out.println();
	}
}
