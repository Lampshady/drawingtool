package com.lampshady.drawing.DrawingTool;



public class Brush {
	private char action;
	private char colour;
	private Point startPoint;
	private Point endPoint;


	public Brush(char action, char colour, Point startPoint, Point endPoint) {
		this.action = action;
		this.colour = colour;
		this.startPoint = startPoint;
		this.endPoint = endPoint;
		
	}

	public char getAction() {
		return action;
	}

	public void setAction(char action) {
		this.action = action;
	}

	public Point getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(Point startPoint) {
		this.startPoint = startPoint;
	}

	public Point getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(Point endPoint) {
		this.endPoint = endPoint;
	}

	public char getColour() {
		return colour;
	}

	public void setColour(char colour) {
		this.colour = colour;
	}
	
	
}