package com.lampshady.drawing.DrawingTool;

public class Point {
    public int x = 0;
    public int y = 0;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

	public Point up() {
		return new Point(x-1, y);
	}
	public Point down() {
		return new Point(x+1, y);
	}
	public Point left() {
		return new Point(x, y-1);
	}

	public Point right() {
		return new Point(x, y+1);
	}

	public boolean sameX(Point point) {
		return x==point.x;
	}
	
	public boolean sameY(Point point) {
		return y==point.y;
	}
}