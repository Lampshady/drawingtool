package com.lampshady.drawing.DrawingTool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class CanvasTest {
	public static char LINECOLOUR = 'x';
	public static char BUCKETCOLOUR = 'x';
	
	private Canvas canvas;
	private char[] blankLine = {' ',' ',' ',' ',' ',' ',' ',' ', ' '};
	private char[] horitonalLine = {LINECOLOUR,LINECOLOUR,LINECOLOUR,LINECOLOUR,LINECOLOUR,LINECOLOUR,' ',' ', ' '};
	private char[] singlePoint = {LINECOLOUR,' ',' ',' ',' ',' ',' ',' ', ' '};

	Point canvasPoint = new Point(9,9);
	Point originPoint = new Point(0,0);
	

	@Before
	public void setUp(){

		canvas = new Canvas(canvasPoint);
	}

	/* Tests drawLine from left to right
	 * Verifies only the line was drawn
	 */
	@Test
	public void testDrawLineForwardsHorizontal() {
		
		canvas.drawLine(LINECOLOUR,originPoint, new Point(5,0));
		assertTrue(doubleArrayEquals(canvas.getDrawingArea(), buildValidHorizontalLine()));
	}
	
	@Test
	public void testDrawLineBackwardsHorizontal() {
		canvas.drawLine(LINECOLOUR, new Point(5,0), originPoint);
		assertTrue(doubleArrayEquals(canvas.getDrawingArea(), buildValidHorizontalLine()));
	}
	
	@Test
	public void testDrawLineForwardsVertical() {
		canvas.drawLine(LINECOLOUR,originPoint, new Point(0,5));
		assertTrue(doubleArrayEquals(canvas.getDrawingArea(), buildValidVerticalLine()));
	}
	
	@Test
	public void testDrawLineBackwardsVertical() {
		canvas.drawLine(LINECOLOUR, new Point(0,5), originPoint);
		assertTrue(doubleArrayEquals(canvas.getDrawingArea(), buildValidVerticalLine()));
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testDrawLineDiagonal() {
		canvas.drawLine(LINECOLOUR, new Point(5,5), originPoint);
	}
	
	@Test
	public void testRectangle() {
		canvas.drawRect(BUCKETCOLOUR, originPoint, new Point(3,3));
		assertTrue(doubleArrayEquals(canvas.getDrawingArea(), buildValidRectangle()));
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testDrawBadRectangle() {
		canvas.drawRect(BUCKETCOLOUR, new Point(3,3), originPoint);
	}


	@Test
	public void testBucket() {
		canvas.drawBucket(BUCKETCOLOUR, originPoint);
		assertEquals(BUCKETCOLOUR, canvas.getDrawingArea()[0][0]);
		assertEquals(BUCKETCOLOUR, canvas.getDrawingArea()[8][8]);
	}
	
	@Test
	public void testBucketOutofBounds() {
		canvas.drawBucket(BUCKETCOLOUR, canvasPoint);
		assertEquals(' ', canvas.getDrawingArea()[0][0]);
		assertEquals(' ', canvas.getDrawingArea()[8][8]);
	}
	
	@Test
	public void testBucketHalfFill() {
		canvas.drawLine(LINECOLOUR, new Point(0,5), new Point(8,5));
		canvas.drawBucket(BUCKETCOLOUR,originPoint);
		assertEquals(BUCKETCOLOUR, canvas.getDrawingArea()[0][0]);
		assertEquals(' ', canvas.getDrawingArea()[8][8]);
	}
	
	private boolean doubleArrayEquals(char[][] one, char[][] two){
		if(one.length!=two.length)return false;
		for(int y=0;y<one.length;y++)
		if (!Arrays.equals(one[y], two[y]))return false;
		return true;
	}

	private char[][] buildValidRectangle() {
		char[][] testRectangle = {	{'x','x','x','x',' ',' ',' ',' ',' '},
									{'x',' ',' ','x',' ',' ',' ',' ',' '},
									{'x',' ',' ','x',' ',' ',' ',' ',' '},
									{'x','x','x','x',' ',' ',' ',' ',' '},
									blankLine,
									blankLine,
									blankLine,
									blankLine,
									blankLine
								};
		return testRectangle;
	}
	
	private char[][] buildValidHorizontalLine() {
		char[][] testRectangle = {	horitonalLine,
									blankLine,
									blankLine,
									blankLine,
									blankLine,
									blankLine,
									blankLine,
									blankLine,
									blankLine
								};
		return testRectangle;
	}
	
	private char[][] buildValidVerticalLine() {
		char[][] testRectangle = {	singlePoint,
									singlePoint,
									singlePoint,
									singlePoint,
									singlePoint,
									singlePoint,
									blankLine,
									blankLine,
									blankLine
								};
		return testRectangle;
	}

}
