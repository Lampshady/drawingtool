package com.lampshady.drawing.DrawingTool;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;


public class ArtistTest {
	private Artist artist;
	private char[][] customerExample = {{'o','o','o','o','o','o','o','o','o','o','o','o','o','o','o','x','x','x','x','x'},
										{'x','x','x','x','x','x','o','o','o','o','o','o','o','o','o','x',' ',' ',' ','x'},
										{' ',' ',' ',' ',' ','x','o','o','o','o','o','o','o','o','o','x','x','x','x','x'},
										{' ',' ',' ',' ',' ','x','o','o','o','o','o','o','o','o','o','o','o','o','o','o'}
								};
	
	@Before
	public void setUp(){
		artist = new Artist();
	}
	
	/* 	Tests invalid then valid input
	 */
	@Test
	public void testInvalidValid() {
		InputStream inputstream;
		try {
			inputstream = new FileInputStream("TestInput\\DoubleBucket.txt");
		} catch (FileNotFoundException e) {
			fail("File Not Found.");
			return;
		}
		System.setIn(inputstream);
		artist.input();
		
		assertEquals('o', artist.getBrush().getColour());
	}
	
	/* 	Tests customer's use case of 5 drawing commands
	 */
	@Test
	public void testCostomerUseCase() {
		drawCommand("C 20 4");
		drawCommand("L 1 2 6 2");
		drawCommand("L 6 3 6 4");
		drawCommand("R 16 1 20 3");
		drawCommand("B 10 3 o");
		
		assertTrue(doubleArrayEquals(artist.getCanvas().getDrawingArea(),customerExample));
	}
	
	@Test
	public void testDoubleCanvas() {
		drawCommand("C 20 4");
		drawCommand("L 1 3 6 3");

		testCostomerUseCase();
	}
	
	/*	A helper function to mock user input and draw request to the canvas
	 */
	private void drawCommand(String inputData){
		System.setIn(new java.io.ByteArrayInputStream(inputData.getBytes()));
		artist.input();
		artist.draw();
	}
	
	/*	A helper function for comparing two 2D char array
	 * 	Returns true if equal 
	 */
	private boolean doubleArrayEquals(char[][] one, char[][] two){
		if(one.length!=two.length)return false;
		for(int y=0;y<one.length;y++)
		if (!Arrays.equals(one[y], two[y]))return false;
		return true;
	}
}
