package com.lampshady.drawing.DrawingTool;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;


public class InspirationUITest {
	private InspirationUI inspirationUI;

	@Before
	public void setUp(){
		inspirationUI = new InspirationUI();
	}
	
	//Parses a valid Create command
	@Test
	public void testValidCreate() {
		String[] parameters = {"C","9","10"};
		assertTrue(inspirationUI.validateInput(parameters));
	}
	
	//Parses a valid Line command
	@Test
	public void testValidLine() {
		String[] parameters = {"L","1","1","1","12"};
		assertTrue(inspirationUI.validateInput(parameters));
	}
	
	//Parses a valid Rectangle command
	@Test
	public void testValidRectangle() {
		String[] parameters = {"R","1","1","13","13"};
		assertTrue(inspirationUI.validateInput(parameters));
	}
	
	//Parses a valid Bucket command
	@Test
	public void testValidBucket() {
		String[] parameters = {"B","1","11","h"};
		assertTrue(inspirationUI.validateInput(parameters));
	}
	
	//Parses a valid Quit command
	@Test
	public void testValidQuit() {
		String[] parameters = {"Q"};
		assertTrue(inspirationUI.validateInput(parameters));
	}
	
	//Fails on an invalid Quit command with non numeric
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidCreate() {
		String[] parameters = {"C","9","r"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid Line command with too many arguments
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidLine() {
		String[] parameters = {"L","1","1","1","12","1"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid Quit command with too few arguments
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidRectangle() {
		String[] parameters = {"R","1","1","13"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid Bucket command with too few arguments
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidBucket() {
		String[] parameters = {"B","1","11"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid Quit command with too many arguments
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidQuit() {
		String[] parameters = {"Q","1"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid command
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidCommand() {
		String[] parameters = {"Z"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on  no command
	@Test(expected=IllegalArgumentException.class)
	public void testEmptyCommand() {
		String[] parameters = {""};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid command that is not a char
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidDoubleCommand() {
		String[] parameters = {"QQ"};
		inspirationUI.validateInput(parameters);
	}
	
	//Fails on an invalid Bucket argument that is not a char
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidDoubleBucket() {
		String[] parameters = {"B","1","11","oo"};
		inspirationUI.validateInput(parameters);
	}
	
}
